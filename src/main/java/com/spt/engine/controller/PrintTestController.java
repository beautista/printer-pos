package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.service.PosService;
import com.spt.engine.service.PrintTestService;


@Controller
public class PrintTestController {
	
	@Autowired
	PrintTestService printTestService;
	
	static final Logger LOGGER = LoggerFactory.getLogger(PrintTestController.class);
	
	@GetMapping( value="/printtest")
	@ResponseBody
	public ResponseEntity<String>  printtest() {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
       
        LOGGER.info("--------------- printtest={}");
        printTestService.printSlip();
		return new ResponseEntity<String>("Test OK", headers, HttpStatus.OK);
	}

}
