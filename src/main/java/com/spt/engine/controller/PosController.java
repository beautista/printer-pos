package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.service.PosService;


@Controller
public class PosController {
	
	@Autowired
	PosService posService;
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };
	Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

	static final Logger LOGGER = LoggerFactory.getLogger(PosController.class);
	
	@CrossOrigin(origins = "*")
	@PostMapping(
			    value="/pos/printSlip/{soNumber}/{saleDate}/{saler}/{itemCount}/{subTotalAmount}/{rebateAmount}/{vatAmount}/{totalAmount}/{tendedAmount}/{changeAmount}", 
			    produces = "text/html;charset=utf-8", 
			    headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  printSlip(
			@PathVariable String soNumber,
			@PathVariable String saleDate,
			@PathVariable String saler,
			@PathVariable String itemCount,
			@PathVariable String subTotalAmount,
			@PathVariable String rebateAmount,
			@PathVariable String vatAmount,
			@PathVariable String totalAmount,
			@PathVariable String tendedAmount,
			@PathVariable String changeAmount,
			HttpServletRequest request) {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        Map<String,String> objectMap = new HashMap<String,String>();;
        objectMap.put("soNumber", soNumber);
        objectMap.put("saleDate", saleDate);
        objectMap.put("saler", saler);
        objectMap.put("itemCount", itemCount);
        objectMap.put("subTotalAmount", subTotalAmount);
        objectMap.put("rebateAmount", rebateAmount);
        objectMap.put("vatAmount", vatAmount);
        objectMap.put("totalAmount", totalAmount);
        objectMap.put("tendedAmount", tendedAmount);
        objectMap.put("changeAmount", changeAmount);
        String itemJson = request.getParameter("item");
        
        LOGGER.info("printSlip={}",request.getParameter("item"));
        
       
        List<Map<String,String>> itemList = gson.fromJson(itemJson, List.class);
        ResponseEntity<String>  responseEntity =  posService.printSlip(objectMap,itemList);
		return new ResponseEntity<String>("", headers, HttpStatus.OK);
	}

}
