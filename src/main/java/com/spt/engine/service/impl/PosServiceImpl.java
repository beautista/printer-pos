package com.spt.engine.service.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spt.engine.service.PosService;

@Service
public class PosServiceImpl implements PosService ,Printable{

	static final Logger LOGGER = LoggerFactory.getLogger(PosServiceImpl.class);
	private final double INCH = 72;
	
	private Map<String,String> objectMap;
	private List<Map<String,String>> objectItem;
	@Override
	public ResponseEntity<String> printSlip(Map<String, String> objectMapData, List<Map<String, String>> itemList) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				HttpHeaders headers = new HttpHeaders();
				headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		        headers.add("Content-Type", "application/json; charset=utf-8");

		        
		        HttpEntity<String> entity = new HttpEntity<String>("", headers);
		        
		        objectMap = objectMapData;
		        objectItem = itemList;
		        
		        //Set print
		        PrinterJob printJob = PrinterJob.getPrinterJob();
		        
		        printJob.setPrintable(this);
		     
		          try {
		             
		              HashPrintRequestAttributeSet attr = new HashPrintRequestAttributeSet();
		              attr.add(new MediaPrintableArea(0f, 0f, 400/72f, 400/72f, MediaPrintableArea.INCH));   

		            printJob.print(attr);
		            
		          } catch (Exception PrintException) {
		            PrintException.printStackTrace();
		          }
				
				return null;
	}

	@Override
	public int print(Graphics g, PageFormat pageFormat, int page)throws PrinterException {
		// TODO Auto-generated method stub
		//objectMap
				// TODO Auto-generated method stub
				 int i;
				 Graphics2D g2d;
				 Line2D.Double line = new Line2D.Double();
				 if (page == 0) {
					  g2d = (Graphics2D) g;
				      g2d.setColor(Color.black);
				 
				      //--- Translate the origin to be (0,0)
				      g2d.translate(15, 0);
				      /* Now we perform our rendering */

				      g.setFont(new Font("Roman", 0, 18));
				      //Support  25 Char
				      LOGGER.info("Start print");
				      g.drawString("    BEAUTISTA", 0, 20);
				      g.drawString("   093-456-4959", 0, 50);
				      g.setFont(new Font("Roman", 0, 8));
				      int lines = 50;
				      int itemLines = 0;
				      g.drawString("DATE:"+String.valueOf(objectMap.get("saleDate")), 0, lines+10);
				      g.drawString("Invoice No."+String.valueOf(objectMap.get("soNumber")), 0, lines+20);
				      g.drawString("CASHIER:"+String.valueOf(objectMap.get("saler")), 0, lines+30);
				      
				      Double itemCount = Double.parseDouble(String.valueOf(objectMap.get("itemCount")));
				      g.drawString("ITEMS:"+String.valueOf(itemCount.intValue()) + " Piece", 0, lines+40);
				      g.drawString("=========================", 0, lines+50);
				      if(objectItem!=null) for(Map<String,String> item:objectItem){
				    	  itemLines += 10;
				    	  g.drawString(String.valueOf(item.get("itemBarode"))+"                    "+String.valueOf(item.get("totalAmount")), 0, lines+itemLines+50);
				      }
				      g.drawString("---------------------------------------------------", 0, lines+itemLines+60);
				      g.drawString("Sub-Total:"+String.valueOf(objectMap.get("subTotalAmount")), 0, lines+itemLines+70);
				      g.drawString("Rebate:"+String.valueOf(objectMap.get("rebateAmount")), 0, lines+itemLines+80);
				      g.drawString("Vat:"+String.valueOf(objectMap.get("vatAmount")), 0, lines+itemLines+90);
				      g.drawString("=========================", 0, lines+itemLines+100);
				      g.drawString("Total:"+String.valueOf(objectMap.get("totalAmount")), 0, lines+itemLines+110);
				      g.drawString("Cash:"+String.valueOf(objectMap.get("tendedAmount")), 0, lines+itemLines+120);
				      g.drawString("Change:"+String.valueOf(objectMap.get("changeAmount")), 0, lines+itemLines+130);
				      g.drawString("---------------------------------------------------", 0, lines+itemLines+140);

				      LOGGER.info("end print");
				      g.drawString("\u0E2A\u0E34\u0E19\u0E04\u0E49\u0E32\u0E0B\u0E37\u0E49\u0E2D\u0E41\u0E25\u0E49\u0E27\u0E44\u0E21\u0E48\u0E23\u0E31\u0E1A\u0E40\u0E1B\u0E25\u0E35\u0E48\u0E22\u0E19", 0, lines+itemLines+150);
				      g.drawString("\u0E2B\u0E23\u0E37\u0E2D\u0E04\u0E37\u0E19\u0E17\u0E38\u0E01\u0E01\u0E23\u0E13\u0E35", 0, lines+itemLines+160);
					  return (PAGE_EXISTS);
				  } else {
				      return (NO_SUCH_PAGE);
				  }
	}

}
