package com.spt.engine.service.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spt.engine.service.PosService;
import com.spt.engine.service.PrintTestService;

@Service
public class PrintTestServiceImpl implements PrintTestService ,Printable{

	static final Logger LOGGER = LoggerFactory.getLogger(PrintTestServiceImpl.class);
	private final double INCH = 72;
	
	private Map<String,String> objectMap;
	private List<Map<String,String>> objectItem;
	@Override
	public ResponseEntity<String> printSlip() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				HttpHeaders headers = new HttpHeaders();
				headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		        headers.add("Content-Type", "application/json; charset=utf-8");

		        
		        HttpEntity<String> entity = new HttpEntity<String>("", headers);
		        
		        //Set print
		        PrinterJob printJob = PrinterJob.getPrinterJob();
		        
		        printJob.setPrintable(this);
		     
		          try {
		             
		              HashPrintRequestAttributeSet attr = new HashPrintRequestAttributeSet();
		              attr.add(new MediaPrintableArea(0f, 0f, 400/72f, 400/72f, MediaPrintableArea.INCH));   

		            printJob.print(attr);
		            
		          } catch (Exception PrintException) {
		            PrintException.printStackTrace();
		          }
				
				return null;
	}

	@Override
	public int print(Graphics g, PageFormat pageFormat, int page)throws PrinterException {
		// TODO Auto-generated method stub
		//objectMap
				// TODO Auto-generated method stub
				 int i;
				 Graphics2D g2d;
				 Line2D.Double line = new Line2D.Double();
				 if (page == 0) {
					  g2d = (Graphics2D) g;
				      g2d.setColor(Color.black);
				 
				      //--- Translate the origin to be (0,0)
				      g2d.translate(15, 0);
				      /* Now we perform our rendering */

				      g.setFont(new Font("Roman", 0, 18));
				      //Support  25 Char
				      LOGGER.info("Start print");
				      g.drawString("    BEAUTISTA", 0, 20);
				      g.drawString("   Test Print", 0, 50);
					  return (PAGE_EXISTS);
				  } else {
				      return (NO_SUCH_PAGE);
				  }
	}

}
