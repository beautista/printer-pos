package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface PosService {

	public ResponseEntity<String> printSlip(Map<String,String> objectMapData,List<Map<String,String>> itemList);
}
