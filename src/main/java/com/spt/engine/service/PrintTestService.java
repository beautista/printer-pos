package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface PrintTestService {

	public ResponseEntity<String> printSlip();
}
